# syntax=docker/dockerfile:experimental
FROM ubuntu:16.04 as clone


# install ssh client and git
RUN apt-get update && apt-get install -y \
	openssh-client \
	git

# download public key for github.com
RUN mkdir -p -m 0600 ~/.ssh && ssh-keyscan github.com >> ~/.ssh/known_hosts

# clone our private repository
RUN --mount=type=ssh git clone git@bitbucket.org:sgs91810/korjaamo.git &&\
    rm -rf korjaamo/.git

